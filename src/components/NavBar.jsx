import React from 'react';
import {Container, Nav, Navbar, NavLink} from "react-bootstrap";
import './NavBar.css'
const NavBar = () => {
    return (
        <>
            <Navbar bg='warning'>
                <Container>
                    <div className='logo'>Moovee</div>
                    <Nav>
                        <NavLink>Home</NavLink>
                        <NavLink>Auth</NavLink>
                    </Nav>
                </Container>
            </Navbar>
        </>
    );
};

export default NavBar;