import React from 'react';
import {Button, Card, Col} from "react-bootstrap";

const ProductItem = ({prop}) => {
    return (
        <Col md={4} className='mt-4'>
            <Card>
                <Card.Img height={150} variant="top" src={prop.img} />
                <Card.Body>
                    <Card.Title>{prop.title}</Card.Title>
                    <Card.Text>
                        {prop.desc}
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="danger">Buy</Button>
                </Card.Body>
            </Card>
        </Col>
    );
};

export default ProductItem;
